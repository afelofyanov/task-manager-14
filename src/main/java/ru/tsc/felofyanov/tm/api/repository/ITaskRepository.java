package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {
    Task create(String name);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task add(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();
}
