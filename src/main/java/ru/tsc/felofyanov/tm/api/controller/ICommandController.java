package ru.tsc.felofyanov.tm.api.controller;

public interface ICommandController {
    void showSystemInfo();

    void showWelcome();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void showAbout();

    void showVersion();

    void showArguments();

    void showCommands();

    void showHelp();
}
