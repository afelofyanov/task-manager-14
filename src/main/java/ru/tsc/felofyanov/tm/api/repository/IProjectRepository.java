package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {
    Project create(String name);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project remove(Project project);

    void clear();

    Project create(String name, String description);

    Project add(Project project);

    boolean existsById(String id);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);
}
