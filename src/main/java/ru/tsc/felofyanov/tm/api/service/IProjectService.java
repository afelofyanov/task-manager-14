package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService {
    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeTaskStatusById(String id, Status status);

    Project changeTaskStatusByIndex(Integer index, Status status);
}
