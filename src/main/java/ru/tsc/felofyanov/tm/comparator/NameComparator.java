package ru.tsc.felofyanov.tm.comparator;

import ru.tsc.felofyanov.tm.api.model.IHasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<IHasName> {

    INSTANCE;

    @Override
    public int compare(final IHasName iHasName, final IHasName t1) {
        if (iHasName == null || t1 == null) return 0;
        if (iHasName.getName() == null || t1.getName() == null) return 0;
        return iHasName.getName().compareTo(t1.getName());
    }
}
