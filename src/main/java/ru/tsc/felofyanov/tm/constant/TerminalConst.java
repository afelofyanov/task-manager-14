package ru.tsc.felofyanov.tm.constant;

public final class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";

    public static final String BIND_TASK_TO_PROJECT = "bind-task-to-project";

    public static final String UNBIND_TASK_FROM_PROJECT = "unbind-task-from-project";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_START_BY_ID = "task-start-by-id";

    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public static final String TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public static final String TASK_START_BY_INDEX = "task-start-by-index";

    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public static final String TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_START_BY_ID = "project-start-by-id";

    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public static final String PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public static final String PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";
}
