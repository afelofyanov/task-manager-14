package ru.tsc.felofyanov.tm.controller;

import ru.tsc.felofyanov.tm.api.controller.ICommandController;
import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.model.Command;
import ru.tsc.felofyanov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());

        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String memoryValue = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory: " + memoryValue);

        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory in JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    @Override
    public void showErrorArgument(String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    @Override
    public void showErrorCommand(String arg) {
        System.err.printf("Error! This command '%s' not supported...\n", arg);
    }

    @Override
    public void showAbout() {
        System.out.println("[About]");
        System.out.println("Name: Alexander Felofyanov");
        System.out.println("E-mail: afelofyanov@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("1.14.0");
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[Help]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands)
            System.out.println(command);
    }
}
